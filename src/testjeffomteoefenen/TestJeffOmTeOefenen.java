/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testjeffomteoefenen;

import java.util.Scanner;

/**
 *
 * @author Jeffrey Jeffrey.de.Boer2@hva.nl
 * Script waar je aantal boeken, boeknamen en boekprijzen invoert.
 * Van die gegevens word het totaal prijs berekend en het duurste boek word gevonden.
 */
public class TestJeffOmTeOefenen {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //define scanner
        Scanner input = new Scanner(System.in);
        

        System.out.println("hoeveel boeken voer je in ?");

        //initialize values om later te gebruiken en vraag naar hoeveelheid boeken.
        int aantalBoeken = input.nextInt();
        int totaal = 0;
        double duurste = 0;
        
        
        //Array voor de titels van boeken en array voor boekprijzen.
        String[] boeken = new String[aantalBoeken];
        double[] boekenPrijzen = new double[aantalBoeken];
        System.out.println("Okay, " + aantalBoeken + " dus !");
        
        //for loop voor de invoer van de boek titels.
        for(int i = 0; i < boeken.length; i++) {
            System.out.println("Voer boek titel in ");
            boeken[i] = input.next(); 
        }
        
        //for loop die invoer terug communiceerd.
        for(int i = 0; i < boeken.length; i++) {
            System.out.println("De titels van uw boeken zijn " + boeken[i]);
        }
            
        //for loop om de prijzen te vragen, met een try catch voor ongeldige invoer.
     try {
        for (int i = 0; i < boekenPrijzen.length; i++) {
            System.out.println("Voer de prijzen in van de boeken ");
            boekenPrijzen[i] = input.nextDouble();
        }
    } catch (Exception e) {
            System.out.println("Dat was een ongeldige invoer, run het script opnieuw");
    }
        
        //for loop om ingevoerde prijzen terug communiceerd.
         for (int i = 0; i < boekenPrijzen.length; i++) {
            System.out.println("Uw ingevoerde prijzen zijn " + boekenPrijzen[i]);
        }
         
         //for loop die totaal prijz van alle boeken berekend.
         for (int i = 0; i < boekenPrijzen.length; i++) {
             totaal += boekenPrijzen[i];
         }
         
         //for loop om de duurste prijs te bepalen.
         for (int i = 0; i < boekenPrijzen.length; i++) {
             if (duurste < boekenPrijzen[i])
                 duurste = boekenPrijzen[i];
         }
         
         for(int i = 0; i < boeken.length; i++)
            System.out.print("Het boek " + boeken[i] + "" + "kost" + " " + boekenPrijzen[i] + "\n");
         
        System.out.println("De totaalprijs van al je boeken is " + totaal);           
        System.out.println("Het duurste boek kost " + duurste);
    }
       
}
